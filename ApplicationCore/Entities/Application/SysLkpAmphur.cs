﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpAmphur
    {
        public string ProvinceCode { get; set; }
        public string AmphurCode { get; set; }
        public string AmphurName { get; set; }
    }
}
