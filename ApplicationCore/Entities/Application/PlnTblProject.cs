﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnTblProject
    {
        public int ProjectId { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string PlanCode { get; set; }
        public short? SubPlan { get; set; }
        public short? Seq { get; set; }
        public short? BudgetSrcId { get; set; }
        public int? OrganId { get; set; }
        public decimal? ProjectValue { get; set; }
        public DateTime? ProjectPermitDate { get; set; }
        public string BudgetCode { get; set; }
        public short? ProcessId { get; set; }
        public int? ProjectTypeId { get; set; }
        public short? ProjectCstatId { get; set; }
        public bool? IsCancel { get; set; }
        public short? ProcMethodId { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? DataOrganId { get; set; }
    }
}
