﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTblReplaceProj
    {
        public int DocId { get; set; }
        public string ProjectCode { get; set; }
        public DateTime? ProjectDate { get; set; }
        public string Title { get; set; }
        public string BudgetYear { get; set; }
        public int? OrganId { get; set; }
        public int? ProjectTypeId { get; set; }
        public int? ProjectStatusId { get; set; }
        public bool? IsApproved { get; set; }
        public string Remark { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
