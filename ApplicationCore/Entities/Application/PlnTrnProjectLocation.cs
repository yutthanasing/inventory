﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnTrnProjectLocation
    {
        public int ProjectId { get; set; }
        public short Seq { get; set; }
        public int OrganId { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsProcured { get; set; }
        public short? ProcStatId { get; set; }
        public int? DataOrganId { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string ExpenseCode { get; set; }
        public string UnitType { get; set; }
        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? OrgExpenseValue { get; set; }
    }
}
