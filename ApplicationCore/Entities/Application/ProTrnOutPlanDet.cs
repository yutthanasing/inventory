﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTrnOutPlanDet
    {
        public int DocId { get; set; }
        public short Seq { get; set; }
        public string ItemDesc { get; set; }
        public string ItemSpec { get; set; }
        public int? Quantity { get; set; }
        public string UnitType { get; set; }
        public decimal? UnitPrice { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsAccepted { get; set; }
        public bool? IsRegistered { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
