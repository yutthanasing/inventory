﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpModule
    {
        public int ModuleCode { get; set; }
        public string ProgramCode { get; set; }
        public int? OrderNum { get; set; }
        public string ModuleName { get; set; }
    }
}
