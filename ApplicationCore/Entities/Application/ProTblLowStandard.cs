﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTblLowStandard
    {
        public int DocId { get; set; }
        public string DocCode { get; set; }
        public int? OrganId { get; set; }
        public DateTime? AcceptDate { get; set; }
        public string ItemSourceType { get; set; }
        public string DiliveryDocCode { get; set; }
        public string ItemDesc { get; set; }
        public string ItemSpec { get; set; }
        public int? Quantity { get; set; }
        public string UnitType { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? VendorId { get; set; }
        public string BudgetYear { get; set; }
        public double? ItemAge { get; set; }
        public double? DeprecRate { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsRegistered { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
