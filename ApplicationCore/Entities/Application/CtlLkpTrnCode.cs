﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpTrnCode
    {
        public string ItemTrnCode { get; set; }
        public string ItemTrnName { get; set; }
    }
}
