﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProLkpProcMethod
    {
        public short ProcMethodId { get; set; }
        public string ProcMethod { get; set; }
        public decimal? LowerCost { get; set; }
        public decimal? UpperCost { get; set; }
        public bool? IsNeed { get; set; }
        public bool? IsBuy { get; set; }
    }
}
