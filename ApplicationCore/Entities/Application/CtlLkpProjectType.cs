﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpProjectType
    {
        public int ProjectTypeId { get; set; }
        public string ProjectType { get; set; }
    }
}
