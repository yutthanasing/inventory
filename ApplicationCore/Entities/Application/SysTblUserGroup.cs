﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysTblUserGroup
    {
        public SysTblUserGroup()
        {
            SysTblGroupPermission = new HashSet<SysTblGroupPermission>();
            SysTblUser = new HashSet<SysTblUser>();
        }

        public int GroupCode { get; set; }
        public string GroupName { get; set; }

        public ICollection<SysTblGroupPermission> SysTblGroupPermission { get; set; }
        public ICollection<SysTblUser> SysTblUser { get; set; }
    }
}
