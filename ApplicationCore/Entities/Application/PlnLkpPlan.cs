﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpPlan
    {
        public string PlanCode { get; set; }
        public short? Seq { get; set; }
        public string PlanName { get; set; }
        public string SubPlanNo { get; set; }
        public short? SubPlan { get; set; }
        public string BudgetYear { get; set; }
    }
}
