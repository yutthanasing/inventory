﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpExpenseType
    {
        public string ExpenseTypeCode { get; set; }
        public string ExpenseTypeName { get; set; }
        public string ExpenseGrpCode { get; set; }
        public short? BudgetSrcId { get; set; }
        public bool? IsGroup { get; set; }
        public bool? IsProcure { get; set; }
    }
}
