﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTblDonate
    {
        public int DocId { get; set; }
        public string DocCode { get; set; }
        public DateTime? DocDate { get; set; }
        public string Title { get; set; }
        public DateTime? DonateDate { get; set; }
        public int? OrganId { get; set; }
        public int VendorId { get; set; }
        public string BudgetYear { get; set; }
        public bool? IsRegistered { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
