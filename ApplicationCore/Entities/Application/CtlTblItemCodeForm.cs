﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTblItemCodeForm
    {
        public int Id { get; set; }
        public int? FormId { get; set; }
        public string ItemCode { get; set; }
    }
}
