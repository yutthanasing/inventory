﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnTblExpense
    {
        public int ExpenseId { get; set; }
        public string ExpenseCode { get; set; }
        public string ExpenseName { get; set; }
        public bool IsItem { get; set; }
        public string ExpenseTypeCode { get; set; }
    }
}
