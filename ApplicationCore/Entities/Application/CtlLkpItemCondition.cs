﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpItemCondition
    {
        public int ItemConditionId { get; set; }
        public string ItemConditionName { get; set; }
        public bool? IshowforControler { get; set; }
        public string ItemTrnCode { get; set; }
    }
}
