﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpProcStat
    {
        public short ProcStatId { get; set; }
        public string ProcStat { get; set; }
    }
}
