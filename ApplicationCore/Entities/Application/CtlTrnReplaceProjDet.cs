﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTrnReplaceProjDet
    {
        public int DocId { get; set; }
        public short Seq { get; set; }
        public int ItemId { get; set; }
        public int ProjectId { get; set; }
        public short? ProjSeq { get; set; }
        public int? ItemIdnew { get; set; }
        public bool? IsReplaced { get; set; }
        public int TranId { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
