﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpProjRepStat
    {
        public int ProjectStatusId { get; set; }
        public string ProjectStatus { get; set; }
    }
}
