﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTrnBuyDet
    {
        public int DocId { get; set; }
        public short Seq { get; set; }
        public int? AgreeDocId { get; set; }
        public string ItemDesc { get; set; }
        public string ItemSpec { get; set; }
        public int? Quantity { get; set; }
        public string UnitType { get; set; }
        public decimal? UnitPrice { get; set; }
        public string ItemCode { get; set; }
        public int? ProjectId { get; set; }
        public short? ProjSeq { get; set; }
        public int? ProjOrganId { get; set; }
        public bool? IsSelected { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
