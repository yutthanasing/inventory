﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpItemGrp
    {
        public string ItemGrpCode { get; set; }
        public string ItemGrp { get; set; }
        public string ExpenseTypeCode { get; set; }
        public double? ItemAge { get; set; }
        public double? DeprecRate { get; set; }
        public int? MaintOrganId { get; set; }
    }
}
