﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpProcess
    {
        public short JobId { get; set; }
        public short ProcessId { get; set; }
        public string Process { get; set; }
        public string ProcessGrp { get; set; }
        public int? OperOrganId { get; set; }
    }
}
