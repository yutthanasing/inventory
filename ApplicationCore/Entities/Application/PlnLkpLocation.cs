﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpLocation
    {
        public int LocationId { get; set; }
        public int? OrganId { get; set; }
        public string LocationAbbr { get; set; }
        public string LocationName { get; set; }
    }
}
