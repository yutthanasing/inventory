﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProLkpVendor
    {
        public int VendorId { get; set; }
        public string VendorClass { get; set; }
        public string VendorName { get; set; }
        public string ItemGrpCode { get; set; }
        public string AuthorName { get; set; }
        public string TaxId { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string ProvinceCode { get; set; }
        public string AmphurCode { get; set; }
        public string TambolCode { get; set; }
        public string EMail { get; set; }
        public bool? IsGovern { get; set; }
        public bool? IsDonator { get; set; }
        public bool? IsNotCenter { get; set; }
        public bool? IsCancel { get; set; }
    }
}
