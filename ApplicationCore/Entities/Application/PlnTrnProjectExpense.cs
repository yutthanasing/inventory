﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnTrnProjectExpense
    {
        public int ProjectId { get; set; }
        public short Seq { get; set; }
        public string BudgetCode { get; set; }
        public string BudgetYear { get; set; }
        public short? ActivityId { get; set; }
        public string ExpenseTypeCode { get; set; }
        public int? ExpenseId { get; set; }
        public string ItemGrpCode { get; set; }
        public string ExpenseDesc { get; set; }
        public decimal? ExpenseValue { get; set; }
        public int? Quantity { get; set; }
        public string UnitType { get; set; }
        public decimal? UnitPrice { get; set; }
        public string Remark { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsProcured { get; set; }
        public int? ProjectReplacementId { get; set; }
        public short? ProcStatId { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? DataOrganId { get; set; }
    }
}
