﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class OldCtlTblItemSpec
    {
        public int ItemId { get; set; }
        public string Itemcode { get; set; }
        public string FldText1 { get; set; }
        public string FldText2 { get; set; }
        public string FldText3 { get; set; }
        public string FldText4 { get; set; }
        public string FldText5 { get; set; }
        public string FldText6 { get; set; }
        public string FldText7 { get; set; }
        public string FldText8 { get; set; }
        public string FldText9 { get; set; }
        public string FldText10 { get; set; }
        public string FldText11 { get; set; }
        public string FldText12 { get; set; }
        public string FldText13 { get; set; }
        public string FldText14 { get; set; }
        public string FldText15 { get; set; }
        public string FldText16 { get; set; }
        public string FldText17 { get; set; }
        public string FldText18 { get; set; }
        public string FldText19 { get; set; }
        public string FldText20 { get; set; }
        public int? FldNum21 { get; set; }
        public int? FldNum22 { get; set; }
        public int? FldNum23 { get; set; }
        public int? FldNum24 { get; set; }
        public int? FldNum25 { get; set; }
        public DateTime? FldDate26 { get; set; }
        public DateTime? FldDate27 { get; set; }
        public DateTime? FldDate28 { get; set; }
    }
}
