﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpJob
    {
        public short JobId { get; set; }
        public string Job { get; set; }
    }
}
