﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpDataType
    {
        public int DataType { get; set; }
        public string Type { get; set; }
        public string FunctionType { get; set; }
    }
}
