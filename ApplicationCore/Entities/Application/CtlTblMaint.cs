﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTblMaint
    {
        public int DocId { get; set; }
        public int ItemId { get; set; }
        public int TranId { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string MaintenanceType { get; set; }
        public string SenderName { get; set; }
        public string SenderTel { get; set; }
        public string Cause { get; set; }
        public int? VendorId { get; set; }
        public DateTime? MaintDate { get; set; }
        public string MaintDocNo { get; set; }
        public decimal? EstimateCost { get; set; }
        public string Remark { get; set; }
        public int? MaintDays { get; set; }
        public DateTime? AcceptDate { get; set; }
        public decimal? NetPrice { get; set; }
        public string InvoiceNo { get; set; }
        public string MaintDetail { get; set; }
        public int? WorkOrganId { get; set; }
        public int? OldItemAge { get; set; }
        public decimal? OldDeprecRate { get; set; }
        public bool? IsCompleted { get; set; }
        public int? DataOrganId { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
