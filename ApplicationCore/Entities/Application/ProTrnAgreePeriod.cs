﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTrnAgreePeriod
    {
        public int DocId { get; set; }
        public short Seq { get; set; }
        public decimal? OperPaid { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? SuccDate { get; set; }
        public bool? IsPaid { get; set; }
    }
}
