﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlItemTranAction
    {
        public int TranId { get; set; }
        public int ItemId { get; set; }
        public short? Seq { get; set; }
        public DateTime TranDate { get; set; }
        public string ItemTrnCode { get; set; }
        public int OriginalOrganId { get; set; }
        public int? OriginalLocationId { get; set; }
        public int? DestinationOrganId { get; set; }
        public int? DestinationLocationId { get; set; }
        public string ItemOrganCode { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public short TranStatus { get; set; }
        public string DocCode { get; set; }
        public string IssueName { get; set; }
        public string ContactTel { get; set; }
        public string DueToCode { get; set; }
        public string Remark { get; set; }
        public bool? IsCurrent { get; set; }
        public bool? IsInformRepair { get; set; }
        public bool? IsCurMaint { get; set; }
        public int? RefId { get; set; }
        public decimal? EstimatePrice { get; set; }
        public decimal? NetPrice { get; set; }
        public bool? IsControllerKnow { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? DataOrganId { get; set; }
    }
}
