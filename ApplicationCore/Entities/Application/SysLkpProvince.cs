﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpProvince
    {
        public string ProvinceCode { get; set; }
        public string Province { get; set; }
        public string ProvinceEname { get; set; }
        public short? Seq { get; set; }
        public string InspectRegion { get; set; }
        public string AreaCode { get; set; }
    }
}
