﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTrnItemValue
    {
        public int ItemId { get; set; }
        public short Seq { get; set; }
        public DateTime? CalDate { get; set; }
        public string ItemDesc { get; set; }
        public bool? IsShowSum { get; set; }
        public double? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalValue { get; set; }
        public double? ItemAge { get; set; }
        public double? DeprecRate { get; set; }
        public decimal? AnnualDeprec { get; set; }
        public decimal? AccumDeprec { get; set; }
        public decimal? NetValue { get; set; }
        public string Remark { get; set; }
        public int? TranId { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
