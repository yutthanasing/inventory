﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysTblGroupPermission
    {
        public int GroupCode { get; set; }
        public int ModuleCode { get; set; }
        public string Permission { get; set; }

        public SysTblUserGroup GroupCodeNavigation { get; set; }
    }
}
