﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpItemStatus
    {
        public int ItemStatusId { get; set; }
        public string ItemStatusName { get; set; }
    }
}
