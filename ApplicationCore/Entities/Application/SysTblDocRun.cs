﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysTblDocRun
    {
        public int RunId { get; set; }
        public string RunCode { get; set; }
        public string RunDecription { get; set; }
        public int? CurrentOrder { get; set; }
        public int? SavedOrder { get; set; }
        public int? MaxOrder { get; set; }
        public bool IsCancel { get; set; }
    }
}
