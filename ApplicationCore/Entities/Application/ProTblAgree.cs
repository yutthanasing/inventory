﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTblAgree
    {
        public int DocId { get; set; }
        public string DocCode { get; set; }
        public int? ProjectId { get; set; }
        public DateTime? AgreeDate { get; set; }
        public int? BuyDocId { get; set; }
        public string BuyDocCode { get; set; }
        public string Title { get; set; }
        public short? Seq { get; set; }
        public int? OwnerOrganId { get; set; }
        public int? VendorId { get; set; }
        public string InvoiceNo { get; set; }
        public string Chairman { get; set; }
        public string Committee1 { get; set; }
        public string Committee2 { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? AgreePrice { get; set; }
        public short? NumPeriod { get; set; }
        public DateTime? EndDate { get; set; }
        public int? WarrantyDay { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Remark { get; set; }
        public short? ProcStatId { get; set; }
        public bool? IsRegistered { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
