﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpTranDueto
    {
        public string DuetoCode { get; set; }
        public string DueToName { get; set; }
        public string ItemTrnCode { get; set; }
    }
}
