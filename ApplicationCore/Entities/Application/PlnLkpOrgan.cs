﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpOrgan
    {
        public int OrganId { get; set; }
        public string OrganCode { get; set; }
        public string OrganName { get; set; }
        public string OrganNamePrt { get; set; }
        public string OrganAbbr { get; set; }
        public string OrganLoc { get; set; }
        public string ProvinceCode { get; set; }
        public bool? IsCentral { get; set; }
    }
}
