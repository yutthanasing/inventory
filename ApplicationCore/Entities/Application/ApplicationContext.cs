﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApplicationCore.Entities.Application
{
    public partial class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CtlItemTranAction> CtlItemTranAction { get; set; }
        public virtual DbSet<CtlLkpDataType> CtlLkpDataType { get; set; }
        public virtual DbSet<CtlLkpItemCondition> CtlLkpItemCondition { get; set; }
        public virtual DbSet<CtlLkpItemGrp> CtlLkpItemGrp { get; set; }
        public virtual DbSet<CtlLkpItemStatus> CtlLkpItemStatus { get; set; }
        public virtual DbSet<CtlLkpItemType> CtlLkpItemType { get; set; }
        public virtual DbSet<CtlLkpProjectType> CtlLkpProjectType { get; set; }
        public virtual DbSet<CtlLkpProjRepStat> CtlLkpProjRepStat { get; set; }
        public virtual DbSet<CtlLkpTranDueto> CtlLkpTranDueto { get; set; }
        public virtual DbSet<CtlLkpTrnCode> CtlLkpTrnCode { get; set; }
        public virtual DbSet<CtlTblItem> CtlTblItem { get; set; }
        public virtual DbSet<CtlTblItemCodeForm> CtlTblItemCodeForm { get; set; }
        public virtual DbSet<CtlTblItemSpec> CtlTblItemSpec { get; set; }
        public virtual DbSet<CtlTblMaint> CtlTblMaint { get; set; }
        public virtual DbSet<CtlTblReplaceProj> CtlTblReplaceProj { get; set; }
        public virtual DbSet<CtlTblSpecTemplate> CtlTblSpecTemplate { get; set; }
        public virtual DbSet<CtlTrnItemPart> CtlTrnItemPart { get; set; }
        public virtual DbSet<CtlTrnItemValue> CtlTrnItemValue { get; set; }
        public virtual DbSet<CtlTrnMaintDet> CtlTrnMaintDet { get; set; }
        public virtual DbSet<CtlTrnReplaceProjDet> CtlTrnReplaceProjDet { get; set; }
        public virtual DbSet<OldCtlTblItem> OldCtlTblItem { get; set; }
        public virtual DbSet<OldCtlTblItemIscentral> OldCtlTblItemIscentral { get; set; }
        public virtual DbSet<OldCtlTblItemSpec> OldCtlTblItemSpec { get; set; }
        public virtual DbSet<OldCtlTblItemSpecIscentral> OldCtlTblItemSpecIscentral { get; set; }
        public virtual DbSet<OldCtlTrnItemValue> OldCtlTrnItemValue { get; set; }
        public virtual DbSet<OldCtlTrnItemValueIscentral> OldCtlTrnItemValueIscentral { get; set; }
        public virtual DbSet<PlnLkpBudgetSrc> PlnLkpBudgetSrc { get; set; }
        public virtual DbSet<PlnLkpExpenseType> PlnLkpExpenseType { get; set; }
        public virtual DbSet<PlnLkpLocation> PlnLkpLocation { get; set; }
        public virtual DbSet<PlnLkpOrgan> PlnLkpOrgan { get; set; }
        public virtual DbSet<PlnLkpPlan> PlnLkpPlan { get; set; }
        public virtual DbSet<PlnLkpProcStat> PlnLkpProcStat { get; set; }
        public virtual DbSet<PlnTblActivity> PlnTblActivity { get; set; }
        public virtual DbSet<PlnTblExpense> PlnTblExpense { get; set; }
        public virtual DbSet<PlnTblProject> PlnTblProject { get; set; }
        public virtual DbSet<PlnTrnProjectExpense> PlnTrnProjectExpense { get; set; }
        public virtual DbSet<PlnTrnProjectLocation> PlnTrnProjectLocation { get; set; }
        public virtual DbSet<ProLkpProcMethod> ProLkpProcMethod { get; set; }
        public virtual DbSet<ProLkpVendor> ProLkpVendor { get; set; }
        public virtual DbSet<ProTblAgree> ProTblAgree { get; set; }
        public virtual DbSet<ProTblBuy> ProTblBuy { get; set; }
        public virtual DbSet<ProTblDonate> ProTblDonate { get; set; }
        public virtual DbSet<ProTblLowStandard> ProTblLowStandard { get; set; }
        public virtual DbSet<ProTblOutPlan> ProTblOutPlan { get; set; }
        public virtual DbSet<ProTrnAgreeDet> ProTrnAgreeDet { get; set; }
        public virtual DbSet<ProTrnAgreePeriod> ProTrnAgreePeriod { get; set; }
        public virtual DbSet<ProTrnBuyDet> ProTrnBuyDet { get; set; }
        public virtual DbSet<ProTrnBuySubDet> ProTrnBuySubDet { get; set; }
        public virtual DbSet<ProTrnDonateDet> ProTrnDonateDet { get; set; }
        public virtual DbSet<ProTrnOutPlanDet> ProTrnOutPlanDet { get; set; }
        public virtual DbSet<SysLkpAmphur> SysLkpAmphur { get; set; }
        public virtual DbSet<SysLkpJob> SysLkpJob { get; set; }
        public virtual DbSet<SysLkpModule> SysLkpModule { get; set; }
        public virtual DbSet<SysLkpProcess> SysLkpProcess { get; set; }
        public virtual DbSet<SysLkpProgram> SysLkpProgram { get; set; }
        public virtual DbSet<SysLkpProvince> SysLkpProvince { get; set; }
        public virtual DbSet<SysLkpTambol> SysLkpTambol { get; set; }
        public virtual DbSet<SysTblDocRun> SysTblDocRun { get; set; }
        public virtual DbSet<SysTblGroupPermission> SysTblGroupPermission { get; set; }
        public virtual DbSet<SysTblUser> SysTblUser { get; set; }
        public virtual DbSet<SysTblUserGroup> SysTblUserGroup { get; set; }

        // Unable to generate entity type for table 'dbo.ProLkpItemFrom'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CtlLkpItemCategory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SysTblModuleFile'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.sysTblInfor'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PlnLkpExpenseOld'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SysLkpModuleTUN'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PlnLkpOrganAll'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SysTblComment'. Please see the warning messages.

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CtlItemTranAction>(entity =>
            {
                entity.HasKey(e => e.TranId);

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.ApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.ContactTel).HasMaxLength(30);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DestinationLocationId).HasColumnName("DestinationLocationID");

                entity.Property(e => e.DestinationOrganId).HasColumnName("DestinationOrganID");

                entity.Property(e => e.DocCode).HasMaxLength(50);

                entity.Property(e => e.DueToCode).HasMaxLength(2);

                entity.Property(e => e.EstimatePrice).HasColumnType("money");

                entity.Property(e => e.IsControllerKnow)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IsCurMaint)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IsCurrent)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IsInformRepair)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IssueName).HasMaxLength(80);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemOrganCode).HasMaxLength(50);

                entity.Property(e => e.ItemTrnCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.NetPrice).HasColumnType("money");

                entity.Property(e => e.OriginalLocationId).HasColumnName("OriginalLocationID");

                entity.Property(e => e.OriginalOrganId).HasColumnName("OriginalOrganID");

                entity.Property(e => e.RefId).HasColumnName("RefID");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.Seq).HasDefaultValueSql("(1)");

                entity.Property(e => e.TranDate).HasColumnType("datetime");

                entity.Property(e => e.TranStatus).HasDefaultValueSql("(0)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<CtlLkpDataType>(entity =>
            {
                entity.HasKey(e => e.DataType);

                entity.Property(e => e.DataType).ValueGeneratedNever();

                entity.Property(e => e.FunctionType).HasMaxLength(20);

                entity.Property(e => e.Type).HasMaxLength(20);
            });

            modelBuilder.Entity<CtlLkpItemCondition>(entity =>
            {
                entity.HasKey(e => e.ItemConditionId);

                entity.Property(e => e.ItemConditionId)
                    .HasColumnName("ItemConditionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ItemConditionName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ItemTrnCode).HasMaxLength(10);
            });

            modelBuilder.Entity<CtlLkpItemGrp>(entity =>
            {
                entity.HasKey(e => e.ItemGrpCode);

                entity.Property(e => e.ItemGrpCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.ExpenseTypeCode).HasMaxLength(10);

                entity.Property(e => e.ItemGrp).HasMaxLength(50);

                entity.Property(e => e.MaintOrganId).HasColumnName("MaintOrganID");
            });

            modelBuilder.Entity<CtlLkpItemStatus>(entity =>
            {
                entity.HasKey(e => e.ItemStatusId);

                entity.Property(e => e.ItemStatusId)
                    .HasColumnName("ItemStatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ItemStatusName)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<CtlLkpItemType>(entity =>
            {
                entity.HasKey(e => e.ItemCode);

                entity.Property(e => e.ItemCode)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.GeneralSpec).HasMaxLength(150);

                entity.Property(e => e.ItemGrpCode).HasMaxLength(20);

                entity.Property(e => e.ItemName).HasMaxLength(250);

                entity.Property(e => e.ItemType).HasMaxLength(50);

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);
            });

            modelBuilder.Entity<CtlLkpProjectType>(entity =>
            {
                entity.HasKey(e => e.ProjectTypeId);

                entity.Property(e => e.ProjectTypeId)
                    .HasColumnName("ProjectTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProjectType).HasMaxLength(30);
            });

            modelBuilder.Entity<CtlLkpProjRepStat>(entity =>
            {
                entity.HasKey(e => e.ProjectStatusId);

                entity.Property(e => e.ProjectStatusId)
                    .HasColumnName("ProjectStatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProjectStatus).HasMaxLength(30);
            });

            modelBuilder.Entity<CtlLkpTranDueto>(entity =>
            {
                entity.HasKey(e => e.DuetoCode);

                entity.Property(e => e.DuetoCode)
                    .HasMaxLength(2)
                    .ValueGeneratedNever();

                entity.Property(e => e.DueToName).HasMaxLength(50);

                entity.Property(e => e.ItemTrnCode).HasMaxLength(30);
            });

            modelBuilder.Entity<CtlLkpTrnCode>(entity =>
            {
                entity.HasKey(e => e.ItemTrnCode);

                entity.Property(e => e.ItemTrnCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.ItemTrnName).HasMaxLength(50);
            });

            modelBuilder.Entity<CtlTblItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.BudgetSrcId).HasColumnName("BudgetSrcID");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.CurrLocationId).HasColumnName("CurrLocationID");

                entity.Property(e => e.CurrOrganId).HasColumnName("CurrOrganID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode).HasMaxLength(20);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.ImportantId)
                    .HasColumnName("ImportantID")
                    .HasMaxLength(30);

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(20);

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemConditionId).HasColumnName("ItemConditionID");

                entity.Property(e => e.ItemDesc).HasMaxLength(60);

                entity.Property(e => e.ItemFromId).HasColumnName("ItemFromID");

                entity.Property(e => e.ItemGrpCode).HasMaxLength(10);

                entity.Property(e => e.ItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.ItemOrganCodeReplace).HasMaxLength(20);

                entity.Property(e => e.ItemPid).HasColumnName("ItemPID");

                entity.Property(e => e.ItemReplaceId).HasColumnName("ItemReplaceID");

                entity.Property(e => e.ItemReplaceStatus).HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemStatusId).HasColumnName("ItemStatusID");

                entity.Property(e => e.ItemValue).HasColumnType("money");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Model).HasMaxLength(90);

                entity.Property(e => e.OldItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.OldItemOrganCode1).HasMaxLength(20);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.PicLinks1).HasMaxLength(50);

                entity.Property(e => e.PicLinks2).HasMaxLength(50);

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.RegisterDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.ResPerson).HasMaxLength(50);

                entity.Property(e => e.SerialNo)
                    .HasColumnName("SerialNO")
                    .HasMaxLength(20);

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<CtlTblItemCodeForm>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FormId).HasColumnName("FormID");

                entity.Property(e => e.ItemCode).HasMaxLength(50);
            });

            modelBuilder.Entity<CtlTblItemSpec>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ItemID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FldDate26).HasColumnType("datetime");

                entity.Property(e => e.FldDate27).HasColumnType("datetime");

                entity.Property(e => e.FldDate28).HasColumnType("datetime");

                entity.Property(e => e.FldText1).HasMaxLength(50);

                entity.Property(e => e.FldText10).HasMaxLength(50);

                entity.Property(e => e.FldText11).HasMaxLength(50);

                entity.Property(e => e.FldText12).HasMaxLength(50);

                entity.Property(e => e.FldText13).HasMaxLength(50);

                entity.Property(e => e.FldText14).HasMaxLength(50);

                entity.Property(e => e.FldText15).HasMaxLength(50);

                entity.Property(e => e.FldText16).HasMaxLength(50);

                entity.Property(e => e.FldText17).HasMaxLength(50);

                entity.Property(e => e.FldText18).HasMaxLength(50);

                entity.Property(e => e.FldText19).HasMaxLength(50);

                entity.Property(e => e.FldText2).HasMaxLength(50);

                entity.Property(e => e.FldText20).HasMaxLength(50);

                entity.Property(e => e.FldText3).HasMaxLength(50);

                entity.Property(e => e.FldText4).HasMaxLength(50);

                entity.Property(e => e.FldText5).HasMaxLength(50);

                entity.Property(e => e.FldText6).HasMaxLength(50);

                entity.Property(e => e.FldText7).HasMaxLength(50);

                entity.Property(e => e.FldText8).HasMaxLength(50);

                entity.Property(e => e.FldText9).HasMaxLength(50);

                entity.Property(e => e.Itemcode).HasMaxLength(50);
            });

            modelBuilder.Entity<CtlTblMaint>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.Cause).HasMaxLength(100);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.EstimateCost).HasColumnType("money");

                entity.Property(e => e.InvoiceNo).HasMaxLength(30);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.MaintDate).HasColumnType("datetime");

                entity.Property(e => e.MaintDetail).HasMaxLength(100);

                entity.Property(e => e.MaintDocNo).HasMaxLength(20);

                entity.Property(e => e.MaintenanceType)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.NetPrice).HasColumnType("money");

                entity.Property(e => e.OldDeprecRate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.RequestDate).HasColumnType("datetime");

                entity.Property(e => e.SenderName).HasMaxLength(60);

                entity.Property(e => e.SenderTel).HasMaxLength(30);

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.WorkOrganId).HasColumnName("WorkOrganID");
            });

            modelBuilder.Entity<CtlTblReplaceProj>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.ProjectCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ProjectDate).HasColumnType("datetime");

                entity.Property(e => e.ProjectStatusId)
                    .HasColumnName("ProjectStatusID")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.ProjectTypeId).HasColumnName("ProjectTypeID");

                entity.Property(e => e.Remark).HasMaxLength(150);

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<CtlTblSpecTemplate>(entity =>
            {
                entity.HasKey(e => e.FormId);

                entity.Property(e => e.FormId).HasColumnName("FormID");

                entity.Property(e => e.Title1).HasMaxLength(50);

                entity.Property(e => e.Title10).HasMaxLength(50);

                entity.Property(e => e.Title11).HasMaxLength(50);

                entity.Property(e => e.Title12).HasMaxLength(50);

                entity.Property(e => e.Title13).HasMaxLength(50);

                entity.Property(e => e.Title14).HasMaxLength(50);

                entity.Property(e => e.Title15).HasMaxLength(50);

                entity.Property(e => e.Title16).HasMaxLength(50);

                entity.Property(e => e.Title17).HasMaxLength(50);

                entity.Property(e => e.Title18).HasMaxLength(50);

                entity.Property(e => e.Title19).HasMaxLength(50);

                entity.Property(e => e.Title2).HasMaxLength(50);

                entity.Property(e => e.Title20).HasMaxLength(50);

                entity.Property(e => e.Title3).HasMaxLength(50);

                entity.Property(e => e.Title4).HasMaxLength(50);

                entity.Property(e => e.Title5).HasMaxLength(50);

                entity.Property(e => e.Title6).HasMaxLength(50);

                entity.Property(e => e.Title7).HasMaxLength(50);

                entity.Property(e => e.Title8).HasMaxLength(50);

                entity.Property(e => e.Title9).HasMaxLength(50);

                entity.Property(e => e.Unit1).HasMaxLength(20);

                entity.Property(e => e.Unit10).HasMaxLength(20);

                entity.Property(e => e.Unit11).HasMaxLength(20);

                entity.Property(e => e.Unit12).HasMaxLength(20);

                entity.Property(e => e.Unit13).HasMaxLength(20);

                entity.Property(e => e.Unit14).HasMaxLength(20);

                entity.Property(e => e.Unit15).HasMaxLength(20);

                entity.Property(e => e.Unit16).HasMaxLength(20);

                entity.Property(e => e.Unit17).HasMaxLength(20);

                entity.Property(e => e.Unit18).HasMaxLength(20);

                entity.Property(e => e.Unit19).HasMaxLength(20);

                entity.Property(e => e.Unit2).HasMaxLength(20);

                entity.Property(e => e.Unit20).HasMaxLength(20);

                entity.Property(e => e.Unit3).HasMaxLength(20);

                entity.Property(e => e.Unit4).HasMaxLength(20);

                entity.Property(e => e.Unit5).HasMaxLength(20);

                entity.Property(e => e.Unit6).HasMaxLength(20);

                entity.Property(e => e.Unit7).HasMaxLength(20);

                entity.Property(e => e.Unit8).HasMaxLength(20);

                entity.Property(e => e.Unit9).HasMaxLength(20);
            });

            modelBuilder.Entity<CtlTrnItemPart>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.Seq });

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.Spec).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.UnitType).HasMaxLength(20);
            });

            modelBuilder.Entity<CtlTrnItemValue>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.Seq });

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.AccumDeprec).HasColumnType("money");

                entity.Property(e => e.AnnualDeprec).HasColumnType("money");

                entity.Property(e => e.CalDate).HasColumnType("datetime");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemDesc).HasMaxLength(100);

                entity.Property(e => e.NetValue).HasColumnType("money");

                entity.Property(e => e.Remark).HasMaxLength(50);

                entity.Property(e => e.TotalValue).HasColumnType("money");

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<CtlTrnMaintDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.Expense).HasColumnType("money");

                entity.Property(e => e.JobDesc).HasMaxLength(100);

                entity.Property(e => e.MaintDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(50);
            });

            modelBuilder.Entity<CtlTrnReplaceProjDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemIdnew).HasColumnName("ItemIDNew");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<OldCtlTblItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.BudgetSrcId).HasColumnName("BudgetSrcID");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.CurrLocationId).HasColumnName("CurrLocationID");

                entity.Property(e => e.CurrOrganId).HasColumnName("CurrOrganID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode).HasMaxLength(20);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.ImportantId)
                    .HasColumnName("ImportantID")
                    .HasMaxLength(30);

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(20);

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemConditionId).HasColumnName("ItemConditionID");

                entity.Property(e => e.ItemDesc).HasMaxLength(60);

                entity.Property(e => e.ItemFromId).HasColumnName("ItemFromID");

                entity.Property(e => e.ItemGrpCode).HasMaxLength(10);

                entity.Property(e => e.ItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.ItemOrganCodeReplace).HasMaxLength(20);

                entity.Property(e => e.ItemPid).HasColumnName("ItemPID");

                entity.Property(e => e.ItemReplaceId).HasColumnName("ItemReplaceID");

                entity.Property(e => e.ItemReplaceStatus).HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemStatusId).HasColumnName("ItemStatusID");

                entity.Property(e => e.ItemValue).HasColumnType("money");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Model).HasMaxLength(90);

                entity.Property(e => e.OldItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.OldItemOrganCode1).HasMaxLength(20);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.PicLinks1).HasMaxLength(50);

                entity.Property(e => e.PicLinks2).HasMaxLength(50);

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.RegisterDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.ResPerson).HasMaxLength(50);

                entity.Property(e => e.SerialNo)
                    .HasColumnName("SerialNO")
                    .HasMaxLength(20);

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<OldCtlTblItemIscentral>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.BudgetSrcId).HasColumnName("BudgetSrcID");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.CurrLocationId).HasColumnName("CurrLocationID");

                entity.Property(e => e.CurrOrganId).HasColumnName("CurrOrganID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode).HasMaxLength(20);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.ImportantId)
                    .HasColumnName("ImportantID")
                    .HasMaxLength(30);

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(20);

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemConditionId).HasColumnName("ItemConditionID");

                entity.Property(e => e.ItemDesc).HasMaxLength(60);

                entity.Property(e => e.ItemFromId).HasColumnName("ItemFromID");

                entity.Property(e => e.ItemGrpCode).HasMaxLength(10);

                entity.Property(e => e.ItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.ItemOrganCodeReplace).HasMaxLength(20);

                entity.Property(e => e.ItemPid).HasColumnName("ItemPID");

                entity.Property(e => e.ItemReplaceId).HasColumnName("ItemReplaceID");

                entity.Property(e => e.ItemReplaceStatus).HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemStatusId).HasColumnName("ItemStatusID");

                entity.Property(e => e.ItemValue).HasColumnType("money");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Model).HasMaxLength(90);

                entity.Property(e => e.OldItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.OldItemOrganCode1).HasMaxLength(20);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.PicLinks1).HasMaxLength(50);

                entity.Property(e => e.PicLinks2).HasMaxLength(50);

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.RegisterDate).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.ResPerson).HasMaxLength(50);

                entity.Property(e => e.SerialNo)
                    .HasColumnName("SerialNO")
                    .HasMaxLength(20);

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<OldCtlTblItemSpec>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ItemID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FldDate26).HasColumnType("datetime");

                entity.Property(e => e.FldDate27).HasColumnType("datetime");

                entity.Property(e => e.FldDate28).HasColumnType("datetime");

                entity.Property(e => e.FldText1).HasMaxLength(50);

                entity.Property(e => e.FldText10).HasMaxLength(50);

                entity.Property(e => e.FldText11).HasMaxLength(50);

                entity.Property(e => e.FldText12).HasMaxLength(50);

                entity.Property(e => e.FldText13).HasMaxLength(50);

                entity.Property(e => e.FldText14).HasMaxLength(50);

                entity.Property(e => e.FldText15).HasMaxLength(50);

                entity.Property(e => e.FldText16).HasMaxLength(50);

                entity.Property(e => e.FldText17).HasMaxLength(50);

                entity.Property(e => e.FldText18).HasMaxLength(50);

                entity.Property(e => e.FldText19).HasMaxLength(50);

                entity.Property(e => e.FldText2).HasMaxLength(50);

                entity.Property(e => e.FldText20).HasMaxLength(50);

                entity.Property(e => e.FldText3).HasMaxLength(50);

                entity.Property(e => e.FldText4).HasMaxLength(50);

                entity.Property(e => e.FldText5).HasMaxLength(50);

                entity.Property(e => e.FldText6).HasMaxLength(50);

                entity.Property(e => e.FldText7).HasMaxLength(50);

                entity.Property(e => e.FldText8).HasMaxLength(50);

                entity.Property(e => e.FldText9).HasMaxLength(50);

                entity.Property(e => e.Itemcode).HasMaxLength(50);
            });

            modelBuilder.Entity<OldCtlTblItemSpecIscentral>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ItemID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FldDate26).HasColumnType("datetime");

                entity.Property(e => e.FldDate27).HasColumnType("datetime");

                entity.Property(e => e.FldDate28).HasColumnType("datetime");

                entity.Property(e => e.FldText1).HasMaxLength(50);

                entity.Property(e => e.FldText10).HasMaxLength(50);

                entity.Property(e => e.FldText11).HasMaxLength(50);

                entity.Property(e => e.FldText12).HasMaxLength(50);

                entity.Property(e => e.FldText13).HasMaxLength(50);

                entity.Property(e => e.FldText14).HasMaxLength(50);

                entity.Property(e => e.FldText15).HasMaxLength(50);

                entity.Property(e => e.FldText16).HasMaxLength(50);

                entity.Property(e => e.FldText17).HasMaxLength(50);

                entity.Property(e => e.FldText18).HasMaxLength(50);

                entity.Property(e => e.FldText19).HasMaxLength(50);

                entity.Property(e => e.FldText2).HasMaxLength(50);

                entity.Property(e => e.FldText20).HasMaxLength(50);

                entity.Property(e => e.FldText3).HasMaxLength(50);

                entity.Property(e => e.FldText4).HasMaxLength(50);

                entity.Property(e => e.FldText5).HasMaxLength(50);

                entity.Property(e => e.FldText6).HasMaxLength(50);

                entity.Property(e => e.FldText7).HasMaxLength(50);

                entity.Property(e => e.FldText8).HasMaxLength(50);

                entity.Property(e => e.FldText9).HasMaxLength(50);

                entity.Property(e => e.Itemcode).HasMaxLength(50);
            });

            modelBuilder.Entity<OldCtlTrnItemValue>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.Seq });

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.AccumDeprec).HasColumnType("money");

                entity.Property(e => e.AnnualDeprec).HasColumnType("money");

                entity.Property(e => e.CalDate).HasColumnType("datetime");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemDesc).HasMaxLength(50);

                entity.Property(e => e.NetValue).HasColumnType("money");

                entity.Property(e => e.Remark).HasMaxLength(50);

                entity.Property(e => e.TotalValue).HasColumnType("money");

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<OldCtlTrnItemValueIscentral>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ItemID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccumDeprec).HasColumnType("money");

                entity.Property(e => e.AnnualDeprec).HasColumnType("money");

                entity.Property(e => e.CalDate).HasColumnType("datetime");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemDesc).HasMaxLength(50);

                entity.Property(e => e.NetValue).HasColumnType("money");

                entity.Property(e => e.Remark).HasMaxLength(50);

                entity.Property(e => e.TotalValue).HasColumnType("money");

                entity.Property(e => e.TranId).HasColumnName("TranID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<PlnLkpBudgetSrc>(entity =>
            {
                entity.HasKey(e => e.BudgetSrcId);

                entity.Property(e => e.BudgetSrcId)
                    .HasColumnName("BudgetSrcID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BudgetName).HasMaxLength(50);
            });

            modelBuilder.Entity<PlnLkpExpenseType>(entity =>
            {
                entity.HasKey(e => e.ExpenseTypeCode);

                entity.Property(e => e.ExpenseTypeCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.BudgetSrcId).HasColumnName("BudgetSrcID");

                entity.Property(e => e.ExpenseGrpCode).HasMaxLength(10);

                entity.Property(e => e.ExpenseTypeName).HasMaxLength(50);
            });

            modelBuilder.Entity<PlnLkpLocation>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.LocationAbbr).HasMaxLength(50);

                entity.Property(e => e.LocationName).HasMaxLength(100);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");
            });

            modelBuilder.Entity<PlnLkpOrgan>(entity =>
            {
                entity.HasKey(e => e.OrganId);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.OrganAbbr).HasMaxLength(50);

                entity.Property(e => e.OrganCode).HasMaxLength(15);

                entity.Property(e => e.OrganLoc).HasMaxLength(50);

                entity.Property(e => e.OrganName).HasMaxLength(50);

                entity.Property(e => e.OrganNamePrt).HasMaxLength(50);

                entity.Property(e => e.ProvinceCode).HasMaxLength(2);
            });

            modelBuilder.Entity<PlnLkpPlan>(entity =>
            {
                entity.HasKey(e => e.PlanCode);

                entity.Property(e => e.PlanCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.PlanName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SubPlanNo).HasMaxLength(50);
            });

            modelBuilder.Entity<PlnLkpProcStat>(entity =>
            {
                entity.HasKey(e => e.ProcStatId);

                entity.Property(e => e.ProcStatId)
                    .HasColumnName("ProcStatID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProcStat).HasMaxLength(30);
            });

            modelBuilder.Entity<PlnTblActivity>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.ActivityId });

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.ActivityId).HasColumnName("ActivityID");

                entity.Property(e => e.ActivityName).HasMaxLength(200);

                entity.Property(e => e.TargetAreaId).HasColumnName("TargetAreaID");

                entity.Property(e => e.TargetUnitCount).HasMaxLength(10);
            });

            modelBuilder.Entity<PlnTblExpense>(entity =>
            {
                entity.HasKey(e => e.ExpenseId);

                entity.Property(e => e.ExpenseId).HasColumnName("ExpenseID");

                entity.Property(e => e.ExpenseCode).HasMaxLength(20);

                entity.Property(e => e.ExpenseName).HasMaxLength(225);

                entity.Property(e => e.ExpenseTypeCode).HasMaxLength(20);
            });

            modelBuilder.Entity<PlnTblProject>(entity =>
            {
                entity.HasKey(e => e.ProjectId);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.BudgetCode).HasMaxLength(20);

                entity.Property(e => e.BudgetSrcId).HasColumnName("BudgetSrcID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.PlanCode).HasMaxLength(20);

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.ProcessId).HasColumnName("ProcessID");

                entity.Property(e => e.ProjectCode).HasMaxLength(20);

                entity.Property(e => e.ProjectCstatId).HasColumnName("ProjectCStatID");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.ProjectPermitDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ProjectTypeId)
                    .HasColumnName("ProjectTypeID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.ProjectValue).HasColumnType("money");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<PlnTrnProjectExpense>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.Seq });

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.ActivityId).HasColumnName("ActivityID");

                entity.Property(e => e.BudgetCode).HasMaxLength(20);

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ExpenseDesc).HasMaxLength(150);

                entity.Property(e => e.ExpenseId).HasColumnName("ExpenseID");

                entity.Property(e => e.ExpenseTypeCode).HasMaxLength(10);

                entity.Property(e => e.ExpenseValue).HasColumnType("money");

                entity.Property(e => e.ItemGrpCode).HasMaxLength(20);

                entity.Property(e => e.ProcStatId)
                    .HasColumnName("ProcStatID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.ProjectReplacementId).HasColumnName("ProjectReplacementID");

                entity.Property(e => e.Remark).HasMaxLength(150);

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<PlnTrnProjectLocation>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.Seq, e.OrganId });

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ExpenseCode).HasMaxLength(20);

                entity.Property(e => e.OrgExpenseValue).HasColumnType("money");

                entity.Property(e => e.ProcStatId).HasColumnName("ProcStatID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProLkpProcMethod>(entity =>
            {
                entity.HasKey(e => e.ProcMethodId);

                entity.Property(e => e.ProcMethodId)
                    .HasColumnName("ProcMethodID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LowerCost).HasColumnType("money");

                entity.Property(e => e.ProcMethod).HasMaxLength(50);

                entity.Property(e => e.UpperCost).HasColumnType("money");
            });

            modelBuilder.Entity<ProLkpVendor>(entity =>
            {
                entity.HasKey(e => e.VendorId);

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.AmphurCode).HasMaxLength(2);

                entity.Property(e => e.AuthorName).HasMaxLength(50);

                entity.Property(e => e.EMail)
                    .HasColumnName("eMail")
                    .HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.IsCancel).HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemGrpCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.ProvinceCode).HasMaxLength(2);

                entity.Property(e => e.TambolCode).HasMaxLength(2);

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasMaxLength(20);

                entity.Property(e => e.VendorClass).HasMaxLength(20);

                entity.Property(e => e.VendorName).HasMaxLength(100);
            });

            modelBuilder.Entity<ProTblAgree>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.AgreeDate).HasColumnType("datetime");

                entity.Property(e => e.AgreePrice).HasColumnType("money");

                entity.Property(e => e.BuyDocCode).HasMaxLength(20);

                entity.Property(e => e.BuyDocId).HasColumnName("BuyDocID");

                entity.Property(e => e.Chairman).HasMaxLength(50);

                entity.Property(e => e.Committee1).HasMaxLength(20);

                entity.Property(e => e.Committee2).HasMaxLength(20);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceNo).HasMaxLength(20);

                entity.Property(e => e.OwnerOrganId).HasColumnName("OwnerOrganID");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.ProcStatId).HasColumnName("ProcStatID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<ProTblBuy>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.AdvertDate).HasColumnType("datetime");

                entity.Property(e => e.AgreeDate).HasColumnType("datetime");

                entity.Property(e => e.AgreePrice).HasColumnType("money");

                entity.Property(e => e.Agreement).HasMaxLength(200);

                entity.Property(e => e.BudgetCode).HasMaxLength(20);

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.BuyDate).HasColumnType("datetime");

                entity.Property(e => e.BuyType).HasMaxLength(20);

                entity.Property(e => e.BuyValue).HasColumnType("money");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DeclareDate).HasColumnType("datetime");

                entity.Property(e => e.DocCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ExpenseTypeCode).HasMaxLength(10);

                entity.Property(e => e.Master).HasMaxLength(20);

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.Present).HasMaxLength(50);

                entity.Property(e => e.Problem).HasMaxLength(50);

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.ProcOfficer).HasMaxLength(20);

                entity.Property(e => e.ProcStatId).HasColumnName("ProcStatID");

                entity.Property(e => e.QuoteDate).HasColumnType("datetime");

                entity.Property(e => e.Reason).HasMaxLength(100);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(100);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProTblDonate>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.DocDate).HasColumnType("datetime");

                entity.Property(e => e.DonateDate).HasColumnType("datetime");

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<ProTblLowStandard>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DiliveryDocCode).HasMaxLength(50);

                entity.Property(e => e.DocCode).HasMaxLength(50);

                entity.Property(e => e.IsSelected).HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemDesc)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemSourceType)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.ItemSpec).HasMaxLength(100);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<ProTblOutPlan>(entity =>
            {
                entity.HasKey(e => e.DocId);

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AcceptDate).HasColumnType("datetime");

                entity.Property(e => e.AgreeDate).HasColumnType("datetime");

                entity.Property(e => e.AgreeDocCode).HasMaxLength(20);

                entity.Property(e => e.AgreePrice).HasColumnType("money");

                entity.Property(e => e.BudgetYear).HasMaxLength(4);

                entity.Property(e => e.BuyDate).HasColumnType("datetime");

                entity.Property(e => e.BuyType).HasMaxLength(20);

                entity.Property(e => e.Chairman).HasMaxLength(20);

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceNo).HasMaxLength(100);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.ProcMethodId).HasColumnName("ProcMethodID");

                entity.Property(e => e.ProcStatId).HasColumnName("ProcStatID");

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.SendDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(100);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<ProTrnAgreeDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AgreeDocId).HasColumnName("AgreeDocID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemDesc).HasMaxLength(200);

                entity.Property(e => e.ItemSpec).HasMaxLength(200);

                entity.Property(e => e.ProjOrganId).HasColumnName("ProjOrganID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SalesPrice).HasColumnType("money");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<ProTrnAgreePeriod>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.OperPaid).HasColumnType("money");

                entity.Property(e => e.SuccDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProTrnBuyDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.AgreeDocId).HasColumnName("AgreeDocID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemDesc).HasMaxLength(200);

                entity.Property(e => e.ItemSpec).HasMaxLength(200);

                entity.Property(e => e.ProjOrganId).HasColumnName("ProjOrganID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProTrnBuySubDet>(entity =>
            {
                entity.HasKey(e => e.ItemPid);

                entity.Property(e => e.ItemPid).HasColumnName("ItemPID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.ItemCode).HasMaxLength(20);

                entity.Property(e => e.ItemDesc).HasMaxLength(100);

                entity.Property(e => e.ItemFromId).HasColumnName("ItemFromID");

                entity.Property(e => e.ItemOrganCode).HasMaxLength(20);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.OwnerOrganId).HasColumnName("OwnerOrganID");

                entity.Property(e => e.Remark).HasMaxLength(50);

                entity.Property(e => e.SerialNo).HasMaxLength(20);

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<ProTrnDonateDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.IsSelected)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.ItemDesc)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemSpec).HasMaxLength(100);

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProTrnOutPlanDet>(entity =>
            {
                entity.HasKey(e => new { e.DocId, e.Seq });

                entity.Property(e => e.DocId).HasColumnName("DocID");

                entity.Property(e => e.DataOrganId).HasColumnName("DataOrganID");

                entity.Property(e => e.ItemDesc)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemSpec).HasMaxLength(100);

                entity.Property(e => e.UnitPrice).HasColumnType("money");

                entity.Property(e => e.UnitType).HasMaxLength(10);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<SysLkpAmphur>(entity =>
            {
                entity.HasKey(e => new { e.ProvinceCode, e.AmphurCode });

                entity.Property(e => e.ProvinceCode).HasMaxLength(2);

                entity.Property(e => e.AmphurCode).HasMaxLength(2);

                entity.Property(e => e.AmphurName).HasMaxLength(50);
            });

            modelBuilder.Entity<SysLkpJob>(entity =>
            {
                entity.HasKey(e => e.JobId);

                entity.Property(e => e.JobId)
                    .HasColumnName("JobID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Job).HasMaxLength(30);
            });

            modelBuilder.Entity<SysLkpModule>(entity =>
            {
                entity.HasKey(e => e.ModuleCode);

                entity.Property(e => e.ModuleCode).ValueGeneratedNever();

                entity.Property(e => e.ModuleName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProgramCode)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<SysLkpProcess>(entity =>
            {
                entity.HasKey(e => new { e.JobId, e.ProcessId });

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.ProcessId).HasColumnName("ProcessID");

                entity.Property(e => e.OperOrganId).HasColumnName("OperOrganID");

                entity.Property(e => e.Process).HasMaxLength(30);

                entity.Property(e => e.ProcessGrp).HasMaxLength(30);
            });

            modelBuilder.Entity<SysLkpProgram>(entity =>
            {
                entity.HasKey(e => e.ProgramCode);

                entity.Property(e => e.ProgramCode)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.ProgramName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProgramType).HasMaxLength(20);
            });

            modelBuilder.Entity<SysLkpProvince>(entity =>
            {
                entity.HasKey(e => e.ProvinceCode);

                entity.Property(e => e.ProvinceCode)
                    .HasMaxLength(2)
                    .ValueGeneratedNever();

                entity.Property(e => e.AreaCode).HasMaxLength(2);

                entity.Property(e => e.InspectRegion).HasMaxLength(4);

                entity.Property(e => e.Province).HasMaxLength(50);

                entity.Property(e => e.ProvinceEname)
                    .HasColumnName("ProvinceEName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SysLkpTambol>(entity =>
            {
                entity.HasKey(e => new { e.ProvinceCode, e.AmphurCode, e.TambolCode });

                entity.Property(e => e.ProvinceCode).HasMaxLength(2);

                entity.Property(e => e.AmphurCode).HasMaxLength(2);

                entity.Property(e => e.TambolCode).HasMaxLength(2);

                entity.Property(e => e.TambolName).HasMaxLength(50);
            });

            modelBuilder.Entity<SysTblDocRun>(entity =>
            {
                entity.HasKey(e => e.RunId);

                entity.Property(e => e.RunId).HasColumnName("RunID");

                entity.Property(e => e.RunCode)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.RunDecription).HasMaxLength(50);
            });

            modelBuilder.Entity<SysTblGroupPermission>(entity =>
            {
                entity.HasKey(e => new { e.GroupCode, e.ModuleCode });

                entity.Property(e => e.Permission)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.HasOne(d => d.GroupCodeNavigation)
                    .WithMany(p => p.SysTblGroupPermission)
                    .HasForeignKey(d => d.GroupCode)
                    .HasConstraintName("FK_SysTblGroupPermission_SysTblUserGroup");
            });

            modelBuilder.Entity<SysTblUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.DateApply).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(60);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Identification)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.OrganCode).HasMaxLength(20);

                entity.Property(e => e.OrganId).HasColumnName("OrganID");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.PreName).HasMaxLength(20);

                entity.Property(e => e.ProvinceCode)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.Tel).HasMaxLength(20);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.XPosition)
                    .HasColumnName("xPosition")
                    .HasMaxLength(60);

                entity.HasOne(d => d.GroupCodeNavigation)
                    .WithMany(p => p.SysTblUser)
                    .HasForeignKey(d => d.GroupCode)
                    .HasConstraintName("FK_SysTblUser_SysTblUserGroup");
            });

            modelBuilder.Entity<SysTblUserGroup>(entity =>
            {
                entity.HasKey(e => e.GroupCode);

                entity.Property(e => e.GroupName).HasMaxLength(150);
            });
        }
    }
}
