﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTblOutPlan
    {
        public int DocId { get; set; }
        public string DocCode { get; set; }
        public int? OrganId { get; set; }
        public DateTime? BuyDate { get; set; }
        public string Title { get; set; }
        public string BuyType { get; set; }
        public string AgreeDocCode { get; set; }
        public DateTime? AgreeDate { get; set; }
        public string VendorId { get; set; }
        public string InvoiceNo { get; set; }
        public string Chairman { get; set; }
        public decimal? AgreePrice { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? WarrantyDay { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Remark { get; set; }
        public int? ProcMethodId { get; set; }
        public short? ProcStatId { get; set; }
        public string BudgetYear { get; set; }
        public bool? IsRegistered { get; set; }
        public bool? IsCancel { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
