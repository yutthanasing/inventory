﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnTblActivity
    {
        public int ProjectId { get; set; }
        public short ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int? TargetQuantity { get; set; }
        public string TargetUnitCount { get; set; }
        public short? TargetAreaId { get; set; }
        public bool IsCancel { get; set; }
    }
}
