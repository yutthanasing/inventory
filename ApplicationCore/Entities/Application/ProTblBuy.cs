﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTblBuy
    {
        public int DocId { get; set; }
        public string DocCode { get; set; }
        public string BudgetYear { get; set; }
        public DateTime? BuyDate { get; set; }
        public string Title { get; set; }
        public string BuyType { get; set; }
        public string Reason { get; set; }
        public string BudgetCode { get; set; }
        public decimal? BuyValue { get; set; }
        public string ExpenseTypeCode { get; set; }
        public short? ProcMethodId { get; set; }
        public string ProcOfficer { get; set; }
        public string Master { get; set; }
        public DateTime? AdvertDate { get; set; }
        public DateTime? QuoteDate { get; set; }
        public DateTime? DeclareDate { get; set; }
        public DateTime? AgreeDate { get; set; }
        public string Agreement { get; set; }
        public decimal? AgreePrice { get; set; }
        public string Present { get; set; }
        public short? NumPeriod { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Problem { get; set; }
        public short? ProcStatId { get; set; }
        public bool? IsCancel { get; set; }
        public int? DataOrganId { get; set; }
        public int? UserId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
