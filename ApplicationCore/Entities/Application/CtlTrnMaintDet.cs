﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTrnMaintDet
    {
        public int DocId { get; set; }
        public short Seq { get; set; }
        public DateTime? MaintDate { get; set; }
        public string JobDesc { get; set; }
        public decimal? Expense { get; set; }
        public string Remark { get; set; }
    }
}
