﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTrnItemPart
    {
        public int ItemId { get; set; }
        public short Seq { get; set; }
        public string Title { get; set; }
        public string Spec { get; set; }
        public string UnitType { get; set; }
        public int? DataType { get; set; }
    }
}
