﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlTblSpecTemplate
    {
        public int FormId { get; set; }
        public int? NumberTitle { get; set; }
        public string Title1 { get; set; }
        public string Unit1 { get; set; }
        public int? DataType1 { get; set; }
        public string Title2 { get; set; }
        public string Unit2 { get; set; }
        public int? DataType2 { get; set; }
        public string Title3 { get; set; }
        public string Unit3 { get; set; }
        public int? DataType3 { get; set; }
        public string Title4 { get; set; }
        public string Unit4 { get; set; }
        public int? DataType4 { get; set; }
        public string Title5 { get; set; }
        public string Unit5 { get; set; }
        public int? DataType5 { get; set; }
        public string Title6 { get; set; }
        public string Unit6 { get; set; }
        public int? DataType6 { get; set; }
        public string Title7 { get; set; }
        public string Unit7 { get; set; }
        public int? DataType7 { get; set; }
        public string Title8 { get; set; }
        public string Unit8 { get; set; }
        public int? DataType8 { get; set; }
        public string Title9 { get; set; }
        public string Unit9 { get; set; }
        public int? DataType9 { get; set; }
        public string Title10 { get; set; }
        public string Unit10 { get; set; }
        public int? DataType10 { get; set; }
        public string Title11 { get; set; }
        public string Unit11 { get; set; }
        public int? DataType11 { get; set; }
        public string Title12 { get; set; }
        public string Unit12 { get; set; }
        public int? DataType12 { get; set; }
        public string Title13 { get; set; }
        public string Unit13 { get; set; }
        public int? DataType13 { get; set; }
        public string Title14 { get; set; }
        public string Unit14 { get; set; }
        public int? DataType14 { get; set; }
        public string Title15 { get; set; }
        public string Unit15 { get; set; }
        public int? DataType15 { get; set; }
        public string Title16 { get; set; }
        public string Unit16 { get; set; }
        public int? DataType16 { get; set; }
        public string Title17 { get; set; }
        public string Unit17 { get; set; }
        public int? DataType17 { get; set; }
        public string Title18 { get; set; }
        public string Unit18 { get; set; }
        public int? DataType18 { get; set; }
        public string Title19 { get; set; }
        public string Unit19 { get; set; }
        public int? DataType19 { get; set; }
        public string Title20 { get; set; }
        public string Unit20 { get; set; }
        public int? DataType20 { get; set; }
    }
}
