﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpTambol
    {
        public string ProvinceCode { get; set; }
        public string AmphurCode { get; set; }
        public string TambolCode { get; set; }
        public string TambolName { get; set; }
    }
}
