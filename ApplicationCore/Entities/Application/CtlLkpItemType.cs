﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class CtlLkpItemType
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string GeneralSpec { get; set; }
        public decimal? UnitPrice { get; set; }
        public string UnitType { get; set; }
        public string ItemGrpCode { get; set; }
        public string ItemType { get; set; }
        public int? ItemCount { get; set; }
        public bool? IsItemSerial { get; set; }
    }
}
