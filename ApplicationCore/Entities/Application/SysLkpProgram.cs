﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysLkpProgram
    {
        public string ProgramCode { get; set; }
        public string ProgramType { get; set; }
        public string ProgramName { get; set; }
    }
}
