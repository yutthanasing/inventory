﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class OldCtlTblItem
    {
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemOrganCode { get; set; }
        public string OldItemOrganCode { get; set; }
        public string OldItemOrganCode1 { get; set; }
        public string ItemGrpCode { get; set; }
        public string SerialNo { get; set; }
        public string ItemDesc { get; set; }
        public string Model { get; set; }
        public int? OrganId { get; set; }
        public int? LocationId { get; set; }
        public int? CurrOrganId { get; set; }
        public int? CurrLocationId { get; set; }
        public int? VendorId { get; set; }
        public string DocCode { get; set; }
        public int? ItemPid { get; set; }
        public int? ItemFromId { get; set; }
        public int? DocId { get; set; }
        public short? Seq { get; set; }
        public short? BudgetSrcId { get; set; }
        public short? ProcMethodId { get; set; }
        public string BudgetYear { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? RegisterDate { get; set; }
        public string UnitType { get; set; }
        public decimal? ItemValue { get; set; }
        public int? WarrantyDay { get; set; }
        public int? ItemAge { get; set; }
        public double? DeprecRate { get; set; }
        public string Ip { get; set; }
        public string ImportantId { get; set; }
        public string PicLinks1 { get; set; }
        public string PicLinks2 { get; set; }
        public string Remark { get; set; }
        public int? ItemStatusId { get; set; }
        public int? ItemConditionId { get; set; }
        public string ItemOrganCodeReplace { get; set; }
        public short? ItemReplaceStatus { get; set; }
        public bool? IsOrganAccepted { get; set; }
        public string ResPerson { get; set; }
        public bool? IsEffective { get; set; }
        public bool? IsOldItem { get; set; }
        public int? ItemReplaceId { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
