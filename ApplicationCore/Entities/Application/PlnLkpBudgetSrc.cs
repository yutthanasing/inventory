﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class PlnLkpBudgetSrc
    {
        public short BudgetSrcId { get; set; }
        public string BudgetName { get; set; }
    }
}
