﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class ProTrnBuySubDet
    {
        public int ItemPid { get; set; }
        public int? ItemFromId { get; set; }
        public int? DocId { get; set; }
        public short? Seq { get; set; }
        public string SerialNo { get; set; }
        public string ItemDesc { get; set; }
        public string Model { get; set; }
        public int? OwnerOrganId { get; set; }
        public int? LocationId { get; set; }
        public int? VendorId { get; set; }
        public double? Quantity { get; set; }
        public string UnitType { get; set; }
        public decimal? UnitPrice { get; set; }
        public string Remark { get; set; }
        public bool? IsExpense { get; set; }
        public bool? IsAccepted { get; set; }
        public bool? IsWithdraw { get; set; }
        public bool? IsRegistered { get; set; }
        public bool? IsRecieved { get; set; }
        public string ItemCode { get; set; }
        public string ItemOrganCode { get; set; }
        public int? UserId { get; set; }
        public int? DataOrganId { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
