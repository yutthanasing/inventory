﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Application
{
    public partial class SysTblUser
    {
        public int UserId { get; set; }
        public int GroupCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PreName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? OrganId { get; set; }
        public string OrganCode { get; set; }
        public string XPosition { get; set; }
        public string ProvinceCode { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public byte? Gender { get; set; }
        public DateTime? DateApply { get; set; }
        public bool IsActive { get; set; }
        public string Identification { get; set; }

        public SysTblUserGroup GroupCodeNavigation { get; set; }
    }
}
