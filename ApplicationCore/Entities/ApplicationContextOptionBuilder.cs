﻿
using ApplicationCore.Entities.Application;

namespace ApplicationCore.Entities
{
    public class ApplicationContextOptionBuilder : OptionBuilder<ApplicationContext>
    {
        public ApplicationContextOptionBuilder(string connectionStrings) : base(connectionStrings)
        {
        }
    }
}
