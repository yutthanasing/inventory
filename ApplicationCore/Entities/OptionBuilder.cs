﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class OptionBuilder<TContext> where TContext : DbContext
    {
        private readonly DbContextOptionsBuilder<TContext> builder;

        public OptionBuilder(string connectionStrings)
        {
            const int maxRetryCount = 5;
            var maxRetryDelay = TimeSpan.FromSeconds(30);
            ICollection<int> errorNumbersToAdd = null;

            builder = new DbContextOptionsBuilder<TContext>();
            builder.UseSqlServer(connectionStrings, sqlOptions =>
            {
                sqlOptions.EnableRetryOnFailure(maxRetryCount, maxRetryDelay, errorNumbersToAdd);
            });
        }

        public DbContextOptions Build()
        {
            return builder.Options;
        }
    }
}
