﻿using Microsoft.AspNetCore.Http;
using System;
namespace Application.MVC.Extensions
{
    public static class HttpContextExtension
    {
        public static T Resolving<T>(this HttpContext context) where T : class
        {
            return context.RequestServices.GetService(typeof(T)) as T;
        }

        public static T Resolving<T>(this IServiceProvider context) where T : class
        {
            return context.GetService(typeof(T)) as T;
        }
    }
}
