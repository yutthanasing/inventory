﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Application.MVC.Attributes
{
    public class PermissionAttribute : ActionFilterAttribute
    {
        public List<Rule> Rules { get; }

        public PermissionAttribute(params Rule[] rule)
        {
            Rules = rule.ToList();
        }
    }
}
