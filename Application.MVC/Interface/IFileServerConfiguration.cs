﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Application.MVC.Interface
{
    public interface IFileServerConfiguration
    {
        DirectoryInfo GetDirectory();

        string GetRequestPrefix();
    }
}
