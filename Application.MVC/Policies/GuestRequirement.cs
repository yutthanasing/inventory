﻿using Application.MVC.Interface;
using Application.MVC.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace Application.MVC.Policies
{
    public class GuestRequirement : AuthorizationHandler<GuestRequirement>, IAuthorizationRequirement
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, GuestRequirement requirement)
        {
            if (context.Resource is AuthorizationFilterContext filterContext)
            {
                var sessionContext = filterContext.HttpContext.Resolving<IUserAuthentication>();
                if (sessionContext.IsLogin())
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    }
}
