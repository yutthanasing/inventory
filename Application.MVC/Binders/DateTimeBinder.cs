﻿using ApplicationCommon.Constants;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Application.MVC.Binders
{
    public class DateTimeBinder : IModelBinder
    {
        private readonly object defaultDateTime;

        public DateTimeBinder(object defaultDateTime)
        {
            this.defaultDateTime = defaultDateTime;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var providerResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (providerResult.Length == 0)
            {
                bindingContext.Result = ModelBindingResult.Success(defaultDateTime);
                return Task.CompletedTask;
            }

            var isSuccess = DateTime.TryParseExact(providerResult.FirstValue,
                ModelBinderFormat.DateTimes,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out DateTime outTime);

            if (isSuccess == false)
            {
                bindingContext.Result = ModelBindingResult.Success(defaultDateTime);
                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(outTime);
            return Task.CompletedTask;
        }
    }
}
