﻿using System;

namespace ApplicationCommon.Extensions
{
    public class DirectoryExistsException : Exception
    {
        public DirectoryExistsException(string message) : base(message)
        {
        }
    }
}
