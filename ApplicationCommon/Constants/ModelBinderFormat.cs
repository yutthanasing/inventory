﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.Constants
{
    public static class ModelBinderFormat
    {
        public static string[] DateTimes => new[]{
            "dd/MM/yyyy",
            "d/M/yyyy", "dd-MM-yyyy",
            "dd/MM/yyyy HH:mm:ss",
            "MMM/yyyy",
            "dd/MM/yyyy HH:mm"
        };
    }
}
