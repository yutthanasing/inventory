﻿using ApplicationCommon.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ApplicationCommon
{
    public static class ThrowIf
    {
        public static void Null<T>(T model, string name) where T : class
        {
            if (model == null)
            {
                throw new ArgumentException($"{name} is null.");
            }
        }

        public static void Null<T>(IEnumerable<T> model, string name) where T : class
        {
            if (model == null)
            {
                throw new ArgumentException($"{name} is null.");
            }
        }

        public static void NullOrEmpty<T>(IEnumerable<T> model, string name) where T : class
        {
            if (model == null || model.Any() == false)
            {
                throw new ArgumentException($"{name} is null or empty.");
            }
        }

        public static void NullOrWhiteSpace(string value, string name)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException($"{name} is null.");
            }
        }

        public static void DirectoryNotFound(string directoryPath)
        {
            if (Directory.Exists(directoryPath) == false)
            {
                throw new DirectoryNotFoundException(directoryPath);
            }
        }

        public static void DirectoryExists(string directoryPath)
        {
            if (Directory.Exists(directoryPath))
            {
                throw new DirectoryExistsException(directoryPath);
            }
        }

        public static void FileNotFound(string directoryPath)
        {
            if (File.Exists(directoryPath) == false)
            {
                throw new FileNotFoundException(directoryPath);
            }
        }
    }
}
